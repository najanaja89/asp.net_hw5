﻿using asp.net_hw5.Models;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace asp.net_hw5.Installers
{
    public class RepositoryInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            var domainPath = AppDomain.CurrentDomain.BaseDirectory;
            var jsonString = "";
            using (StreamReader reader = new StreamReader($"{domainPath}/App_Data/MOCK_DATA.json"))
            {
                string json = reader.ReadToEnd();
                jsonString = json;
            }

            container.Register(Component.For<IRepository<MOCK_DATA>>().ImplementedBy<SQLRepository>().DependsOn(Dependency.OnValue("jsonString", jsonString)).LifestyleTransient());
        }
    }
}