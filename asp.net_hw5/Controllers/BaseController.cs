﻿using asp.net_hw5.Models;
using asp.net_hw5.Modules;
using Ninject;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace asp.net_hw5.Controllers
{
    public class BaseController : Controller
    {
        protected IKernel kernelJsonRepository;
        protected IKernel kernelSQLRepository;
        public BaseController()
        {

            kernelJsonRepository = new StandardKernel(new JsonRepositoryModule());
            kernelSQLRepository = new StandardKernel(new SQLRepositoryModule()); ;
        }
    }
}