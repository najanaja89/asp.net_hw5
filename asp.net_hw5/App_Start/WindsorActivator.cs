using System;
using WebActivatorEx;

[assembly: PreApplicationStartMethod(typeof(asp.net_hw5.App_Start.WindsorActivator), "PreStart")]
[assembly: ApplicationShutdownMethodAttribute(typeof(asp.net_hw5.App_Start.WindsorActivator), "Shutdown")]

namespace asp.net_hw5.App_Start
{
    public static class WindsorActivator
    {
        static ContainerBootstrapper bootstrapper;

        public static void PreStart()
        {
            bootstrapper = ContainerBootstrapper.Bootstrap();
        }
        
        public static void Shutdown()
        {
            if (bootstrapper != null)
                bootstrapper.Dispose();
        }
    }
}