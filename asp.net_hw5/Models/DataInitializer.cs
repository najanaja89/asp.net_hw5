﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace asp.net_hw5.Models
{
    public class DataInitializer : CreateDatabaseIfNotExists<DataContext>
    {
        protected override void Seed(DataContext context)
        {
            var listUsers = new List<MOCK_DATA>
            {
                new MOCK_DATA { first_name = "Bruce", last_name = "Wayne", email = "wayne@wayne.inc", gender="male"},
                new MOCK_DATA { first_name = "Barry", last_name = "Allen", email = "allen@CPD.org", gender="male"},
                new MOCK_DATA { first_name = "Oliver", last_name = "Queen", email = "queen@Queen.inc", gender="male"},
                new MOCK_DATA { first_name = "Ray", last_name = "Palmer", email = "palmer@palmer.inc", gender="male"},
                new MOCK_DATA { first_name = "Barbara", last_name = "Gordon", email = "oracle@wayne.inc", gender="female"},
                new MOCK_DATA { first_name = "Clark", last_name = "Kent", email = "kent@dailystar.org", gender="male"},
                new MOCK_DATA { first_name = "Lex", last_name = "Luthor", email = "luthor@lex.inc", gender="male"},
                new MOCK_DATA { first_name = "Lois", last_name = "Lane", email = "lois@dailystar.org", gender="female"},
            };

            context.MOCK_DATA.AddRange(listUsers);
            context.SaveChanges();
            base.Seed(context);
        }

    }
}