﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace asp.net_hw5.Models
{
    public class SQLRepository : IRepository<MOCK_DATA>
    {

        private DataContext context;

        public SQLRepository()
        {
            this.context = new DataContext();
        }
        public void Create(MOCK_DATA item)
        {
            context.MOCK_DATA.Add(item);
        }

        public void Delete(int? id)
        {
            var data = context.MOCK_DATA.Find(id);
            if(data!=null) context.MOCK_DATA.Remove(data);
        }

        private bool disposed = false;
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public MOCK_DATA GetPerson(int? id)
        {
            return context.MOCK_DATA.Find(id);
        }

        public IEnumerable<MOCK_DATA> GetPersonList()
        {
            return context.MOCK_DATA.ToList();
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public void Update(MOCK_DATA item)
        {
            context.Entry(item).State = EntityState.Modified;
        }
    }
}