﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.IO;

namespace asp.net_hw5.Models
{
    public class JSONRepository : IRepository<MOCK_DATA>
    {
        private List<JsonData> _jsonData;

        public JSONRepository(string jsonString)
        {
            var jsonData = JsonConvert.DeserializeObject<List<JsonData>>(jsonString);
            _jsonData = jsonData;
        }

        public void Create(MOCK_DATA item)
        {
            int id = 0;
            if (_jsonData.FirstOrDefault() != null)
            {
                id = _jsonData.Select(i => i.Id).ToList().Max();
            }

            item.id = id;
            var jsonData = new JsonData()
            {
                Id = item.id + 1,
                Email = item.email,
                FirstName = item.first_name,
                LastName = item.last_name,
                Gender = item.gender
            };

            _jsonData.Add(jsonData);
        }

        public void Delete(int? id)
        {
            var item = _jsonData.Where(i => i.Id == id).FirstOrDefault();
            _jsonData.Remove(item);
        }

        public MOCK_DATA GetPerson(int? id)
        {
            var jsonData = _jsonData.Where(i => i.Id == id).FirstOrDefault();
            var item = new MOCK_DATA
            {
                id = (int)jsonData.Id,
                gender = jsonData.Gender.ToString(),
                email = jsonData.Email,
                first_name = jsonData.FirstName,
                last_name = jsonData.LastName
            };
            return item;
        }

        public IEnumerable<MOCK_DATA> GetPersonList()
        {
            var listItem = new List<MOCK_DATA>();
            foreach (var item in _jsonData)
            {
                listItem.Add(new MOCK_DATA
                {
                    id = (int)item.Id,
                    gender = item.Gender.ToString(),
                    email = item.Email,
                    first_name = item.FirstName,
                    last_name = item.LastName
                });
            }
            return listItem;
        }

        public void Save()
        {
            var domainPath = AppDomain.CurrentDomain.BaseDirectory;
            using (StreamWriter file = File.CreateText($"{domainPath}/App_Data/MOCK_DATA.json"))
            {
                var serializer = new JsonSerializer();
                serializer.Serialize(file, _jsonData);
            }
        }

        public void Update(MOCK_DATA item)
        {
            foreach (var itemJson in _jsonData)
            {
                if (itemJson.Id == item.id)
                {
                    itemJson.Email = item.email;
                    itemJson.FirstName = item.first_name;
                    itemJson.LastName = item.last_name;
                    itemJson.Gender = item.gender;


                }
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~JSONRepository()
        // {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion

    }
}