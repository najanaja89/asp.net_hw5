﻿using asp.net_hw5.Models;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace asp.net_hw5.Modules
{
    public class SQLRepositoryModule : NinjectModule
    {
        public override void Load()
        {
            Bind(typeof(IRepository<MOCK_DATA>)).To(typeof(SQLRepository));
        }
    }
}