﻿using asp.net_hw5.Models;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace asp.net_hw5.Modules
{
    public class JsonRepositoryModule : NinjectModule
    {

        public override void Load()
        {
            var domainPath = AppDomain.CurrentDomain.BaseDirectory;
            var jsonString = "";
            using (StreamReader reader = new StreamReader($"{domainPath}/App_Data/MOCK_DATA.json"))
            {
                string json = reader.ReadToEnd();
                jsonString = json;
            }
            //Bind(typeof(IRepository<MOCK_DATA>)).To(typeof(JSONRepository));
            Bind(typeof(IRepository<MOCK_DATA>)).To<JSONRepository>().WithConstructorArgument<string>(jsonString);
        }
    }
}